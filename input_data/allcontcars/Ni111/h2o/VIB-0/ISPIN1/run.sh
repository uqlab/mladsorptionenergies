#!/bin/bash
#SBATCH --job-name="VIB-0"
#SBATCH --output="vasp.%j.%N.out"
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --export=ALL
#SBATCH -t 06:00:00
#SBATCH -A soc110

mpirun -n 24  /home/mzare/bin/vasp_vtst_5_3 > log



