
## Contents

This package contains the code and data used to generate the results in the following manuscript. 

A. J. Chowdhury, W. Yang, E. Walker, O. Mamun, A. Heyden, and G. A. Terejanu, "Prediction of Adsorption Energies for Chemical Species on Metal Catalyst Surfaces Using Machine Learning", The Journal of Physical Chemistry C, vol. 122, iss. 49, pp. 28142-28150, 2018. doi:10.1021/acs.jpcc.8b09284

## Author

This code has been written by *Asif Chowdhury* (asifc@email.sc.edu), while a PhD candidate at University of South Carolina.

## Instructions to run the code

1. Get the input files from this location: \input_data\SUCC_Pt111_contcars
2. Copy the folder '\SUCC_Pt111_contcars' to your working directory.
3. Place the 'Chemical_Data_Preparation.ipynb' notebook file in the working directory.
4. Run the first two cells of the notebook.
5. Then copy the following code in a new cell of that notebook and run the cell (4 csv files will be generated in the working directory):

```python
encode = ChemDataPrepare(maxAtoms = {'C':4,'H':10,'O':4,'M':64}, 
                         E_H = -3.381787065, E_O = -7.459292679999999, E_C = -10.50690401, 
                         CleanEnergiesForMetals = {'Pt':-404.05855849}, numberOfMetalAtomsInEachLayer = 16, excludeMetal = False,
                         metalAtomicNumType = MetalAtomicNumber.Log, isVerbose = True)
encode.PrepareStructs({'Pt':'SUCC_Pt111_contcars'}, ['Pt'], {'Pt':[0.0]}, 193, EnergyPicker.Minimum, None,
                      'Succinic_subnet_struct.pickle', 'SUCC_Energies.csv')
encode.EncodeUsingCM('Succinic_subnet_struct.pickle', 'SUCC_CM_inclMetal.csv', 
                     4, True, 3.5)
encode.EncodeUsingBoB('Succinic_subnet_struct.pickle', 'SUCC_BoB_inclMetal.csv', 
                      -1, 4, True, 3.5)
encode.EncodeUsingBondCounts('Succinic_subnet_struct.pickle', 'SUCC_BondCount.csv')
```

To run the predictions:

1. Open the ipython notebook file named 'Chemical_Energy_Predictions_latest.ipynb'
2.  Run the first 3 cells. These will load the required functions.
3. Now run the last cell which will run predictions for the bond count input files generated above.

## License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

## Acknowledgment
This material is based upon work supported by the National Science Foundation
under Grant No. DMREF-1534260.